/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

import java.util.Scanner;

/**
 *
 * @author werapan
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TableMap map = new TableMap(20, 20);
        Robot robot = new Robot(2, 2, 'x', map);
        Bomb bomb = new Bomb(5,5);
        map.setBomb(bomb);
        map.setRobot(robot);
        map.addObj(new Tree(10,10));
        map.addObj(new Tree(3,10));
        map.addObj(new Tree(9,10));
        map.addObj(new Tree(11,10));
        map.addObj(new Tree(5,10));
        map.addObj(new Tree(6,10));
        map.addObj(new Tree(7,10));
        map.addObj(new Tree(8,10));
        map.addObj(new Tree(9,10));
        map.addObj(new Tree(9,14));  
        map.addObj(new Tree(2,11));
        while(true) {
            map.showMap();
            // W,a| N,w| E,d| S, s| q: quit
            char direction = inputDirection(sc);
            if(direction=='q') {
                printByeBye();
                break;
            }
            robot.walk(direction);
        }
        
    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner sc) {
        String str = sc.next();
        return str.charAt(0);
    }
}
